# WeatherCity


Desenvolver uma aplicação WEB que permite cadastrar municípios e exibir a previsão do tempo. 


No projeto utilizei o template do Metronic, criando um CRUD da entidade Municípios.
Na listagem de municípios foi utilizado o plugin Datatables e componte Vue.js que consomem uma API interna que por sua vez consome a API do OpenWeatherMap.org, evitando CORS.
No comando "Tempo agora" o sistema leva o usuário para mostrar os dados de forma mais completa do retorno da API em um tile feito também em Vue.Js. Webpack também foi utilizado para gerar o bundle do meu javascript.
Também há um projeto voltado testes automatizados para testar a API.
Foi utilizado migration nos modelos de entidade e aplicado ao banco de dados.

Espero que este trabalho seja de grande valia para a comunidade.


---------------------------------------------------------------


Develop a WEB application that allows you to register citys and display the weather forecast.


In the project it is used the Metronic template, creating a CRUD of the City entity.
In the city table was used the plugin Datatables and compon Vue.js that consume an internal API which in turn consumes the OpenWeatherMap.org API, avoiding CORS.
In the "Time Now" command, the system prompts the user to display the fuller API return data on a tile made in Vue.Js. Webpack was also used to generate my javascript bundle.
There is also a project aimed at automated testing to test the API.
Migration was used in entity models and applied to the database.

I hope this work will be useful to community.


---------------------------------------------------------------


Aplicação na Azure:
http://weathercity.azurewebsites.net/

Docker Hub:
https://hub.docker.com/r/cesaralbuquerque/weathercity