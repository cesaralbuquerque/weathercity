﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WeatherCity.Models
{

	public class Municipio
	{

		public int Id { get; set; }

		[Required(ErrorMessage = "O campo Nome deve ser preenchido.")]
		public string Nome { get; set; }

		[Display(Name = "Inserido em")]
		public DateTime? DataCriacao { get; set; }

		[Display(Name = "Atualizado em")]
		public DateTime? DataAlteracao { get; set; }
	}
}
