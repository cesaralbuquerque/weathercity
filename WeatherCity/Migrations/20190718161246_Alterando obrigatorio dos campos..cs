﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WeatherCity.Migrations
{
    public partial class Alterandoobrigatoriodoscampos : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Nome",
                table: "Municipios",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Nome",
                table: "Municipios",
                nullable: true,
                oldClrType: typeof(string));
        }
    }
}
