﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Flurl;
using Flurl.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WeatherCity.Api
{
	[Route("api/[controller]")]
	public class OpenWeatherMapController : Controller
	{

		const string apiId = "d048d59feca6b05ae14e0b58bf149cad";

		// GET api/OpenWeatherMap/<municipio>
		[HttpGet("{municipio}")]
		public JsonResult Get(string municipio)
		{
			//https://flurl.dev/
			//Flurl is a modern, fluent, asynchronous, testable, portable, buzzword-laden URL builder and HTTP client library for .NET.
			//Exemplo de chamada https://api.openweathermap.org/data/2.5/weather?appid=d048d59feca6b05ae14e0b58bf149cad&q=blumenau&units=metric&units=metric
			var url = "https://api.openweathermap.org/data/2.5/weather"
				.SetQueryParams(new
				{
					appid = apiId,
					q = municipio,
					units = "metric"
				});

			var apiResult = CallUrl(url);

			return Json(apiResult.Result);
		}

		public async Task<dynamic> CallUrl(string url)
		{
			return await url.GetJsonAsync();
		}

	}
}
