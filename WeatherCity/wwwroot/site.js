import Vue from 'vue';
import VueResorce from 'vue-resource';

Vue.use(VueResorce);
Vue.http.options.root = 'http://localhost:64970';

window.Vue = Vue;

const requireComponent = require.context(
	'./Components', true, /\w+\.(vue)$/
);
requireComponent.keys().forEach(fileName => {
	const componentConfig = requireComponent(fileName);
	const componentName = fileName.replace(/^\.\/(.*)\.\w+$/, '$1');

	Vue.component(
		componentName,
		componentConfig.default || componentConfig
	);
});


var quickSearchInit = function (){

	$("#quick-search").submit(function (event) {
		event.preventDefault();

		$('.quick-weather-tile').empty();

		$('#quick-seach-modal').modal('show');
		let city = $('#quick-search-city').val();
		$('.quick-weather-tile').append(`<weather city='${city}'></weather>`);
		new Vue({
			el: '#quick-weather'
		});
	});
};

window.QuickSearchInit = quickSearchInit;


var weatherTableInit = function () {
	var table = $('#weather-table');

	var fixedHeaderOffset = 0;
	if (App.getViewPort().width < App.getResponsiveBreakpoint('md')) {
		if ($('.page-header').hasClass('page-header-fixed-mobile')) {
			fixedHeaderOffset = $('.page-header').outerHeight(true);
		}
	} else if ($('.page-header').hasClass('navbar-fixed-top')) {
		fixedHeaderOffset = $('.page-header').outerHeight(true);
	} else if ($('body').hasClass('page-header-fixed')) {
		fixedHeaderOffset = 64; // admin 5 fixed height
	}

	var oTable = table.dataTable({

		// Internationalisation. For more info refer to http://datatables.net/manual/i18n
		"language": {
			"aria": {
				"sortAscending": ": activate to sort column ascending",
				"sortDescending": ": activate to sort column descending"
			},
			"emptyTable": "No data available in table",
			"info": "Showing _START_ to _END_ of _TOTAL_ entries",
			"infoEmpty": "No entries found",
			"infoFiltered": "(filtered1 from _MAX_ total entries)",
			"lengthMenu": "_MENU_ entries",
			"search": "Search:",
			"zeroRecords": "No matching records found"
		},

		// Or you can use remote translation file
		"language": {
			url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
		},

		// setup rowreorder extension: http://datatables.net/extensions/fixedheader/
		fixedHeader: {
			header: true,
			headerOffset: fixedHeaderOffset
		},
		"orderClasses": false,
		"columnDefs": [
			//N�o ordenar a coluna Temperatura e A��es
			{ "orderable": false, "targets": 3 },
			{ "orderable": false, "targets": 4 }
		],
		"info": false,
		"paging": false,
		"autoWidth": true,
		responsive: true,
		rowReorder: {
			selector: 'td:nth-child(2)'
		},

		// Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
		// setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js). 
		// So when dropdowns used the scrollable div should be removed. 
		"dom": "<'row' <'col-md-12'T>><'row'<''l><'col-md-12 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",
	});
}

window.WeatherTableInit = weatherTableInit;