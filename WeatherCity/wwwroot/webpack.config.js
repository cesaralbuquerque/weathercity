﻿var path = require('path');

const VueLoaderPlugin = require('vue-loader/lib/plugin')

module.exports = {
	module: {
		rules: [
			{
				test: /\.vue$/,
				loader: 'vue-loader'
			}
		]
	},
	resolve: {
		alias: {
			'vue$': 'vue/dist/vue.esm.js' // 'vue/dist/vue.common.js' for webpack 1
		}
	},
	plugins: [
		new VueLoaderPlugin()
	],
	mode: 'development',
	entry: './site.js',
	output: {
		path: path.resolve(__dirname, 'dist'),
		filename: 'site.bundle.js'
	}
};