﻿using FluentAssertions;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace WebApplicationTests
{

	/// <summary>
	/// Teste de API da Web Application 
	/// Referencia: https://medium.com/@higorluis/testes-de-integra%C3%A7%C3%A3o-com-net-core-2-1-xunit-e-fluentassertions-a64bf65084ab
	/// </summary>
	public class ApiControllerTest
	{
		private readonly TestContext _testContext;

		public ApiControllerTest()
		{
			_testContext = new TestContext();
		}

		[Fact]
		public async Task Values_Get_ReturnsOkResponse()
		{
			var response = await _testContext.Client.GetAsync("/api/OpenWeatherMap/blumenau");
			response.EnsureSuccessStatusCode();
			response.StatusCode.Should().Be(HttpStatusCode.OK);
		}

		[Theory]
		[InlineData("Blumenau")]
		[InlineData("Joaçaba")]
		[InlineData("Balneário Camboriú")]
		[InlineData("Catanduvas")]
		public async Task Values_GetById_ValuesReturnsOkResponse(string city)
		{
			
			var response = await _testContext.Client.GetStringAsync($"/api/OpenWeatherMap/{city}");

			var responseObject = JsonConvert.DeserializeObject<dynamic>(response);
			
			Xunit.Assert.Equal(city, ((dynamic)responseObject).name.Value);
			Xunit.Assert.NotNull(((dynamic)responseObject).main.temp.Value);
		}
	}
}
